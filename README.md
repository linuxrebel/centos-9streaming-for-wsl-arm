# CentOS-9s Linux for WSL on ARM

Installs a working CentOS-9s Linux into WSL for ARM.  This is extremely light weight and operates smoother than Ubuntu on Windows 11/10 ARM. More fun is that because it's Linux you can do so much more.  This env has Bash and the basic utils you expect out of Linux.  APK is ready to roll as well.  


## Requirements
 - Windows 10 or 11 for ARM is required. if you want CentOS-9s for x86/amd64 please use the one in the Microsoft Store.
 - Windows 10 Fall Creators Update x64 or later.
 - Windows Subsystem for Linux feature enabled.

## Install Steps

  1. Download the zip file from the releases page (above)
  2. Unzip the contents into a directory in you home. 
  3. Open powershell (admin not needed)
  4. In powershell cd into the CentOS-9s (it will be a longer name) directory you just created
  5. Once in the CentOS-9s dir you should see the following files
     - CentOS-9s.exe
     - rootfs.tar.gz
     - setup.ps1
     - uninstall.ps1
  6. execute the setup.ps1 script by typing it's name at the prompt and hitting return. It will now do the following
     - create a dir in C:\Users\[your win user]\AppData\Local\ called wsl
     - create a dir in wsl called CentOS-9s
     - copy rootfs.tar.gz and CentOS-9s.exe into this new dir
     - run the CentOS-9s.exe to install CentOS-9s into WSL
     - this auto creates th vhd disk for wsl 2
     - create a new user in CentOS-9s that is the same as the user you are logged into Windows as (if you username is billyg then billyg will be your CentOS-9s username)
     - Set a password for your new CentOS-9s User (it will ask you for a password, standard Linux rules apply)
     - Setup your user so that you can sudo 
     - create a link in your Linux home directory to your windows home directory.($HOME/winhome links to /mnt/c/Users/[your win user] )
  7.  Profit.  You can now run "wsl -d CentOS-9s" and get into your new distro.  If you don't have any other wsl distro "wsl" will automatically choose CentOS-9s

## Uninstall

  1.  cd into the original folder you created during install
  2.  Run the uninstall.ps1 command
  3.  It will ask you to typ y to confirm
  4.  CentOS-9s Linux is now removed from your system.

## Other things you can do with CentOS-9s.exe
## How-to-Use(for Installed Instance)
#### exe Usage

Usage:
  1. (no args)
     - Open a new shell with your default settings.


  2. run (command line)
     - Run the given command line in that distro. Inherit current directory.


  3. runp (command line (includes windows path))
     - Run the path translated command line in that distro.


  4. config [setting [value]]
     - `--default-user <user>`: Set the default user for this distro to <user>
     - `--default-uid <uid>`: Set the default user uid for this distro to <uid>
     - `--append-path <on|off>`: Switch of Append Windows PATH to $PATH
     - `--mount-drive <on|off>`: Switch of Mount drives


  5. get [setting]
     - `--default-uid`: Get the default user uid in this distro
     - `--append-path`: Get on/off status of Append Windows PATH to $PATH
     - `--mount-drive`: Get on/off status of Mount drives
     - `--lxguid`: Get WSL GUID key for this distro


  6. backup [contents]
     - `--tgz`: Output backup.tar.gz to the current directory using tar command
     - `--reg`: Output settings registry file to the current directory
     

  7. help
      - Print this usage message.

The CentOS-9s.exe is a binary borrowed from the project here. https://github.com/yuk7/wsldl
