  $pwd = (Resolve-Path .\).Path
  $user = $env:USERNAME
  $wsldir = "C:\Users\$user\AppData\Local\wsl\CentOS-9s"

if ( -not (Test-Path -LiteralPath $wsldir)) {
  try {
         New-Item -Path $wsldir -ItemType Directory -ErrorAction Stop | Out-Null 
      }
  catch {
          Write-Error -Message "Unable to create directory '$wsldir' . Error was: $_" -ErrorAction Stop
          exit 4 
        }
  "Successfully created directory '$wsldir'."
}
  else {
         "Directory already exists"
          exit 2
  }
echo "Copying files to working dir"
if ( -not (Test-Path -LiteralPath $wsldir\CentOS-9s.exe)) {
         cp .\CentOS-9s.exe $wsldir
       }
else {
      "CentOS-9s.exe exists"
       }
if ( -not (Test-Path -LiteralPath $wsldir\rootfs.tar.gz)) {
        cp .\rootfs.tar.gz $wsldir
      }
  else {
         rm $wsldir\rootfs.tar.gz
         cp .\rootfs.tar.gz $wsldir
       }
cd $wsldir
echo "Now running the wsl install script"
.\CentOS-9s.exe 
Start-Sleep 2
echo "Now setting default User"
wsl.exe -d CentOS-9s -e /sbin/useradd $user
Start-Sleep 2
wsl.exe -d CentOS-9s -e /bin/passwd $user
Start-Sleep 2
echo "adding user to group wheel"
wsl.exe -d CentOS-9s -e /sbin/usermod -a -G wheel $user
wsl.exe -d CentOS-9s -u root /bin/bash -c "echo $user ALL=\(ALL\) NOPASSWD: ALL >> /etc/sudoers.d/20-$user"
wsl.exe -d CentOS-9s -e /bin/ln -s /mnt/c/Users/$user /home/$user/winhome
Start-Sleep 2
echo "Setting default user"
.\CentOS-9s.exe config --default-user $user
cd $pwd
echo "Install complete"
