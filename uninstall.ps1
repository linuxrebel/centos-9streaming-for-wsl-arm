  $pwd = (Resolve-Path .\).Path
  $user = $env:USERNAME
  $wsldir = "C:\Users\$user\AppData\Local\wsl\CentOS-9s"

cd $wsldir
.\CentOS-9s.exe  clean 
    if ( ( Test-Path -LiteralPath $wsldir\rootfs) -or ( Test-Path -LiteralPath $wsldir\ext4.vhdx)) {
        echo "User did not remove from WSL"
     }
    else { 
        cd ..\
        Remove-Item $wsldir -Force -Recurse
     }
cd $pwd
